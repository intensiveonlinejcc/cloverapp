package models

import "time"

type Product struct {
	ID         int `gorm:"not null;uniqueIndex;primary_key" json:"id"`
	Petani     User
	PetaniID   int       `gorm:"index"`
	NamaProduk string    `gorm:"size:255;not null" json:"nama_produk"`
	Deskripsi  string    `gorm:"size:255" json:"deskripsi"`
	Foto       string    `gorm:"size:255" json:"foto"`
	Stok       int       `gorm:"not null" json:"stok"`
	Harga      int       `gorm:"not null" json:"harga"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
}
