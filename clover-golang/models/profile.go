package models

import "time"

type Profile struct {
	ID           int `gorm:"not null;uniqueIndex;primary_key" json:"id"`
	User         User
	UserID       int       `gorm:"index"`
	NoKTP        string    `gorm:"size:16;not null;uniqueIndex" json:"no_ktp"`
	NamaLengkap  string    `gorm:"size:100;not null" json:"nama_lengkap"`
	Alamat       string    `gorm:"size:255;not null" json:"alamat"`
	Kota         string    `gorm:"size:100;not null" json:"kota"`
	KodePos      string    `gorm:"size:5;not null" json:"kode_pos"`
	TempatLahir  string    `gorm:"size:100;not null" json:"tempat_lahir"`
	TglLahir     time.Time `gorm:"not null" json:"tgl_lahir"`
	JenisKelamin string    `gorm:"size:20;not null" json:"jenis_kelamin"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}
