package models

import "time"

type Item struct {
	ID            int `gorm:"not null;uniqueIndex;primary_key" json:"id"`
	TransactionID int `gorm:"index"`
	Product       Product
	ProductID     int       `gorm:"index"`
	Qty           int       `gorm:"not null" json:"qty"`
	Subtotal      int       `gorm:"not null" json:"subtotal"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
}
