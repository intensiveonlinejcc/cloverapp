package config

import "clover-golang/models"

type Model struct {
	Model interface{}
}

func RegisterModels() []Model {
	return []Model{
		{Model: models.User{}},
		{Model: models.Profile{}},
		{Model: models.Validation{}},
		{Model: models.Product{}},
		{Model: models.Transaction{}},
		{Model: models.Item{}},
	}
}
