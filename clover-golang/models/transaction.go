package models

import "time"

type Transaction struct {
	ID           int `gorm:"not null;uniqueIndex;primary_key" json:"id"`
	User         User
	UserID       int       `gorm:"index"`
	TglTransaksi time.Time `gorm:"not null" json:"tgl_transaksi"`
	TotalHarga   int       `gorm:"not null" json:"total_harga"`
	Item         []Item
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}
