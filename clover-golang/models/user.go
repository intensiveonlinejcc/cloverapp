package models

import "time"

type User struct {
	ID        int       `gorm:"not null;uniqueIndex;primary_key" json:"id"`
	Username  string    `gorm:"size:100;not null;uniqueIndex" json:"username"`
	Email     string    `gorm:"size:100;uniqueIndex" json:"email"`
	NoHP      string    `gorm:"size:13" json:"no_hp"`
	Password  string    `gorm:"size:255;not null" json:"password"`
	Role      string    `gorm:"size:100;not null" json:"role"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
