package models

import "time"

type Validation struct {
	ID            int `gorm:"not null;uniqueIndex;primary_key" json:"id"`
	Profile       Profile
	ProfileID     int       `gorm:"index"`
	WaktuValidasi time.Time `gorm:"not null" json:"waktu_validasi"`
	ScanKTP       string    `gorm:"size:255;not null" json:"scan_ktp"`
	FotoDiri      string    `gorm:"size:255;not null" json:"foto_diri"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
}
